import PyPDF2 as pp
import sys
dimensions = {"a4": (595.276, 841.89)}

def getDimensions(source):
    sourcePage = source.getPage(0)
    w = sourcePage.mediaBox.getWidth()
    h = sourcePage.mediaBox.getHeight()
    return (w, h)

def getPerPage(sourceDimensions, targetType):
    (w1, h1) = sourceDimensions
    (w2, h2) = dimensions[targetType]
    return (int(int(w2) // w1), int(int(h2) // h1))

def getOffsetForCenter(sourceDimensions, rowscolumns, targetType):
    (w1, h1) = sourceDimensions
    (w2, h2) = dimensions[targetType]
    (r, c) = rowscolumns
    return ((int(w2)-w1*r)/2, (int(h2)-h1*c)/2)

def run(inputName, outputName, targetType, center = True):
    inputFile = open(filename, "rb")
    reader = pp.PdfFileReader(inputFile)
    (sourceW, sourceH) = getDimensions(reader)
    (n, m) = getPerPage((sourceW, sourceH), "a4")
    numPages = n*m
    (w, h) = dimensions["a4"]
    newPages = []
    if center:
        (offsetX, offsetY) = getOffsetForCenter((sourceW, sourceH), (n, m), "a4")
    else:
        (offsetX, offsetY) = (0, 0)
    for nr, page in zip(range(reader.getNumPages()), reader.pages):
        if (nr % numPages) == 0:
            newPages += [pp.pdf.PageObject.createBlankPage(None, width=w, height=h)]
        nrOnPage = nr % numPages
        x = offsetX + (nrOnPage % n) * sourceW
        y = offsetY + (nrOnPage // n) * sourceH
        newPages[-1].mergeTranslatedPage(page, x, y)
    outWriter = pp.PdfFileWriter()
    for page in newPages:
        outWriter.addPage(page)
    out = open("out.pdf", "wb")
    outWriter.write(out)

if __name__ == "__main__":
    filename = sys.argv[1]
    print("Opening", filename)
    run(filename, "out.pdf", "a4")
