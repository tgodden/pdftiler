This small script merges PDF of weird, small dimensions to a single A4.
Useful for printing cards of odd sizes (e.g. playing cards) before cutting them out.

If you want to use another set of dimensions, you can add it to the dict and
change the default "a4" string. 

Usage:
`python3 merge.py inputfile.pdf`
This produces a file "out.pdf", which contains the result.